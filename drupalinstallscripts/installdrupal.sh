#!/bin/bash
echo "Please enter your site folder name"
read  sitename
#echo  "Please enter your database name"
#read  database
database=$sitename
echo "Please enter your account email"
read email
echo "start downloading drupal..."
drush dl drupal --drupal-project-rename=$sitename 
cd  $sitename
echo "Done with downloading drupal ..."
drush site-install standard --db-url=mysql://root:123456@localhost/$database --site-name=$sitename --account-mail=$email  --account-name=admin --account-pass=admin  -y
chmod  777 -R  sites/default/files 

echo "Start installing modules"
enable_modules=('admin_menu' 'admin_menu_toolbar' 'admin_devel' 'module_filter' 'token' 'ctools' 'pathauto' 'ds' 'devel' 'views' 'context' 'views_ui'  'advanced_help')
disable_modules=('firephp' 'overlay' 'toolbar' 'color' 'dashboard')
for dmodule in ${disable_modules[@]}; do
	sudo drush pm-disable $dmodule -y
done

for emodule in ${enable_modules[@]}; do 
	sudo drush dl $emodule -y 
done;

for emodule in ${enable_modules[@]}; do 
	sudo drush pm-enable $emodule -y 
done;
sudo drush dl examples
echo  "Start installing  Zen"
drush dl zen
drush en  zen  -y
drush  cc all
zensuffix="_zen"
zenthemename=$sitename$zensuffix
drush  zen $zenthemename  $zenthemename
drush  en $zenthemename  -y
drush  vset theme_default  $zenthemename
echo "Done, $sitename Drupal site was successfully created!!"
